<?php


	/**
	 *
	 *   FlaskHTTP
	 *   The HTTP multi-part request part
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\FlaskHTTP;


	class HttpRequestPart
	{


		/**
		 *   The content
		 *   @var string
		 *   @access public
		 */

		public $content = null;


		/**
		 *   Content-type
		 *   @var string
		 *   @access public
		 */

		public $contentType = null;


		/**
		 *   Part parameters
		 *   @var array
		 *   @access public
		 */

		public $param = array();


		/**
		 *   Transfer encoding
		 *   @var string
		 *   @access public
		 */

		public $contentTransferEncoding = null;


		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param string $content Content
		 *   @param string $contentType Content-type
		 *   @param array $params Additional parameters
		 *   @param bool $binary Binary part?
		 *   @throws HttpRequestException
		 *   @return HttpRequestPart
		 *
		 */

		public function __construct( $content, $contentType=null, $params=null, $binary=false )
		{
			// Check & set parameters
			if ($content===null) throw new HttpRequestException('Missing parameter: $content');
			if ($contentType!==null && empty($contentType)) throw new HttpRequestException('Missing parameter: $contentType');
			if ($params!==null && !is_array($params)) throw new HttpRequestException('Invalid parameter: $params must be an array');
			$this->content=$content;
			if (!empty($contentType)) $this->contentType=$contentType;
			if (!empty($params)) $this->setParams($params);
			if (!empty($binary)) $this->contentTransferEncoding='binary';
		}


		/**
		 *
		 *   Set parameter
		 *   -------------
		 *   @access public
		 *   @param string $param Parameter name
		 *   @param mixed $value Value
		 *   @return HttpRequestPart
		 *
		 */

		public function setParam( $param, $value )
		{
			$this->param[$param]=$value;
			return $this;
		}


		/**
		 *
		 *   Set parameters
		 *   --------------
		 *   @access public
		 *   @param array $params Parameters
		 *   @return HttpRequestPart
		 *
		 */

		public function setParams( $params )
		{
			foreach ($params as $k => $v)
			{
				$this->param[$k]=$v;
			}
			return $this;
		}


		/**
		 *
		 *   Get part
		 *   --------
		 *   @access public
		 *   @param string $boundary Boundary delimiter
		 *   @return string
		 *
		 */

		public function getPart( $boundary )
		{
			// Delimiter
			$delimiter='-------------'.$boundary;

			// Init
			$data='--'.$delimiter."\n";
			$data.='Content-disposition: form-data';
			foreach ($this->param as $param => $value)
			{
				$data.='; '.$param.'='.$value;
			}
			$data.="\r\n";

			// Content-type
			if (!empty($this->contentType))
			{
				$data.='Content-type: '.$this->contentType;
			}

			// Transfer encoding
			if (!empty($this->contentTransferEncoding))
			{
				$data.='Content-Transfer-Encoding: '.$this->contentTransferEncoding;
			}

			// Empty line
			$data.="\r\n";

			// Content
			$data.=$this->content."\r\n";
		}


	}


?>