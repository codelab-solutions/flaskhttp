<?php


	/**
	 *
	 *   FlaskHTTP
	 *   The HTTP request exception
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\FlaskHTTP;


	class HttpRequestException extends \Exception
	{
	}


?>